Use file lab_report.doc to submit your works.

https://tsaoalbert@bitbucket.org/tsaoalbert/master-template-algebra.git

Rules of Arithmetic Operators for Rationl Numbers
  Given x = a/b, y = c/d,

  x+y = (a*d+c*b) / ( b*d)
  x-y = (a*d-c*b) / ( b*d)
  x*y = (a*c) / ( b*d)
  x/y = (a*d) / ( b*c)

  x==y  iff   a*d==c*b
  x<=y  iff   a*d<=c*b


Rules of arithmetic operators for complext numbers
  Given x = a+b_i, and y = c+d_i

  |x| = a*a+b*b
  |y| = c*c+d*d
  x+y = (a+b_i)+(c+d_i) = (a+c) + (b+d)_i
  x-y = (a+b_i)-(c+d_i) = (a-c) + (b-d)_i
  x*y = (a+b_i)*(c+d_i) = (a*c-b*d) + (a*d+b*c)_i
  x/y = (a+b_i)/(c-d_i) = (a*c+bd)/(c^2+d^2) + (bc - ad)_i /(c^2+d^2)

  x==y  iff   |x| == |y|
  x<=y  iff   |x| <= |y|
