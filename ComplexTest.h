#pragma once
using namespace std;

/*
TODO:
Unit Tests for the following overloaded operators for Complex Numbers.
a)  Arithmetic operators +,-,*,/,+=,-=,*=,/=
b)  Unary operators +,-
c)  Relational operators ==, !=, <, <=, >, >= , and
d)  Prefix and Postfix Increment/Decrement ++ --
e)  Big 3: Copy Cstr, Assignment operator=,  and Dstr
f)  double modulus() that will return the modulus, or magnitude of the complex number.
    Given Complex <int> x = a+b_i, then x.modulus() will return  sqrt ( a*a+b*b) ;
g)  toString() - This will return a string like "(-23.4+8.9_)" to the client.
    Do not provide a show() or display() method.
    You will do this using your insertion operator as shown below.
h)  insertion operator


TODO: Regression Test:
a) Create 10 Complex Numbers and sort the numbers in terms of their magnitude.
b) Create at least one of your own regression test.

*/

