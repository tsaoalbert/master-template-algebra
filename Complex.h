#pragma once
/*
Rules of arithmetic operators for complext numbers
  Given x = a+b_i and y = c+d_i

  |x| = a*a+b*b
  |y| = c*c+d*d
  x+y = (a+b_i)+(c+d_i) = (a+c) + (b+d)_i
  x-y = (a+b_i)-(c+d_i) = (a-c) + (b-d)_i
  x*y = (a+b_i)*(c+d_i) = (a*c-b*d) + (a*d+b*c)_i
  x/y = (a+b_i)/(c-d_i) = (a*c+bd)/(c^2+d^2) + (bc - ad)_i /(c^2+d^2)

  x==y  iff   |x| == |y|
  x<=y  iff   |x| <= |y|
*/

/*
TODO: Implement the following for Complex Numbers.
a)  Arithmetic operators +,-,*,/,+=,-=,*=,/=
b)  Unary operators +,-
c)  Relational operators ==, !=, <, <=, >, >= , and 
d)  Prefix and Postfix Increment/Decrement ++ -- 
e)  Big 3: Copy Cstr, Assignment operator=,  and Dstr
f)  double() that will return the modulus, or magnitude of the complex number.  
    Given Complex <int> x = a+b_i, then x.modulus() will return  sqrt ( a*a+b*b) ;
g)  toString() - This will return a string like "(-23.4+8.9_)" to the client.  
    Do not provide a show() or display() method.  
    You will do this using your insertion operator as shown below.
h)  insertion operator
*/

/*
 Rules of arithmetic operators for complext numbers
 Complex <int> x = a+b_i
 Complex <int> y = c+d_i
 x+y = (a+b_i)+(c+d_i) = (a+c) + (b+d)_i
 x-y = (a+b_i)-(c+d_i) = (a-c) + (b-d)_i
 x*y = (a+b_i)*(c+d_i) = (a*c-b*d) + (a*d+b*c)_i
 x/y = (a+b_i)/(c-d_i) = (a*c+bd)/(c^2+d^2) + (bc - ad)_i /(c^2+d^2)
*/

/*
  Examples:

(1, 2)  + (3, 4)  = (4, 6) 
(1, 2)  - (3, 4)  = (-2, -2) 
(1, 2)  * (3, 4)  = (-5, 10) 
(1, 2)  / (3, 4)  = (0.44, 0.08) 
(1, 2)  + 10 = (11, 2) 
10 / (3, 4)  = (1.2, -1.6) 
*/


#include "Rational.h"
#include <iostream>
#include <cstddef>

using namespace std;



template <typename T>
class Complex
{
public:
  // member function members that belong to the class. The friend
  // function below gives the << operator for ostreams (including cout)
  // the ability to output a Complex object by accessing its member data.

  ostream& display( ostream &out = cout) const ;

  friend  ostream & operator<< <T> (ostream &out, const Complex<T> & c) {
    return out;
  }


  //TODO: add assignment operator=
  //TODO: add copy cstr

	Complex (const T&  r=0, const T&  i=0);  // also provides default constructor
  // copy  cstr
	Complex (const Complex<T> & ) = default;  // 

	// = assignment operator
	Complex<T>& operator= (const Complex&  right) = default;   // = assignment operator

  Complex<T> add (const Complex<T>& right) const ;
	Complex<T> operator+  (const Complex<T>&  right) const ;    // + addition operator
	const Complex<T>& operator+= (const Complex<T>&  right);   // += addition assignment operator
	Complex<T> operator-  (const Complex<T>&  right) const ;    // subtraction operator
	const Complex<T>&  operator-= (const Complex<T>&  right);   // subtraction assignment operator

  //TODO: add binary arithmetic overloadded operators *, *=, /, /=, 
  //TODO: add unary  arithmetic overloadded operators -, +
  //TODO: add overloadded relational operators ==, <=, >=, !=

	Complex<T> operator*  (const Complex<T>&  right) const ;    // + addition operator

  const Complex& setComplex (T n, T d);

  //TODO: add helper function to_string 
  string to_string () const { return ""; } ;

  operator double() const; // compute the modulus 

  //TODO: add friend overloadded insertion  operator << 
  //TODO: add friend overloadded extraction  operator >>


private:
	T m_real;
	T m_imaginary;
};

