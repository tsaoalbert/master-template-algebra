// Rational.cpp : Defines the entry point for the console application.
// Create a Rational class defination
//    Rational(numerator, denominator)
//
// #include "stdafx.h"	    // only for Microsoft Visual Studio C++
#include "Rational.h"   // double quotes = find file in project folder
#include "RationalTest.h"   // double quotes = find file in project folder
#include "Complex.h"   // double quotes = find file in project folder
#include "ComplexTest.h"   // double quotes = find file in project folder
#include <iostream>     // angle brackets = find file in compiler folder
#include <fstream>     // angle brackets = find file in compiler folder
#include <typeinfo>     // 
using namespace std;

// function prototypes

/*
TODO: 

Add codes to test the following overloaded operators for both Rational Numbers and Complex Numbers.
a)  Arithmetic operators +,-,*,/,+=,-=,*=,/=
b)  Relational operators ==, !=, <, <=, >, >= , and 
c)  operator double() 

*/

/*

• Each of your programs has to include a comment block at the top that 
   shows the 
  --  Name of the program (e.g., template complext numbers(.
  --  C++ Compiler Version (e.g. MS visual studio)
  --  Your name,
  --  Student id
  --  Date
  --  Platform (PC, Mac, or Linux machine)  

*/


int main(int argc, char* argv[])
{
  // unitTest ( "output.txt" );
  // unitTestComplex<int>( ) ;
  unitTestRandomRational<int>( ) ;

  // unitTestTelescoping <int> () ;

  // unitTestComplexNumberProduct();
  // unitTestRationalNumberProduct(100) ;

  return 0;
}

