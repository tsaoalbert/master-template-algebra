#pragma once

#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iterator>
#include <set>
#include "Rational.h"

using namespace std;

/*
TODO: 

Add codes to test the following overloaded operators for Rational Numbers.
a)   Arithmetic operators +,-,*,/,+=,-=,*=,/=
b)   Unary operators +,-
c)  Relational operators ==, !=, <, <=, >, >= , and 
d)  operator double() 

*/

// Forward declare Rational template.
template <typename T> class Rational ;

template <typename T>
ostream& operator<< (ostream &out, const Rational<T> & c);

template <typename T>
Rational<T>  operator+ (const T & , const Rational<T> & );

template <class T>
void unitTestRationalNumbers() ;

// function prototypes
template <class T>
void initializeNumbers (Rational<T> &, Rational<T> &, Rational<T> &);

template <class T>
void displayNumbers (const char *, Rational<T>, Rational<T>, Rational<T>);

template <class T>
void unitTestTelescoping ()
{

  cout << "Entering function " << __func__ << std::endl;
  Rational<T> sum ;
  size_t n = 1000 ;
  for (size_t i=1; i < n; ++i ) {
    Rational<T> r (1, i*(i+1));
    sum += r; 

    if ( i < 30 ) {
      cout << r ;
      if ( i !=n-1) cout << " + " ;
      if (i%10==0) cout << endl;
    } else if ( i == 30 ) {
      cout << "..." << endl;
    }
  }
  cout << endl;
  cout << "sum = " << sum << endl;
}

template <class T>
void unitTestRationalNumbers()
{
  cout << "Entering function " << __func__ << std::endl;
//  class    object
//    |        |
//    V        V
    Rational<T> n1;
    Rational<T> n2;  
    Rational<T> n3;      

    cout << endl << endl << "**** n2 = n2.add(n3);  n2.display();  // n2 should become 17/12" << endl;
    initializeNumbers (n1, n2, n3);
    displayNumbers("Before", n1, n2, n3);


    n2 = n2.add(n3);    // n2 + n3 = 3/4 + 2/3 = 9/12 + 8/12 = 17/12
    displayNumbers("After ", n1, n2, n3);
    cout << "n2.display() shows only n2  ";
    n2.display();           //   using the display( ) member function
    cout << endl;

    cout << endl << endl << "**** n1 = n2 + n3; // n1 should become 17/12. Others unchanged" << endl;
    initializeNumbers (n1, n2, n3);
    displayNumbers("Before", n1, n2, n3);
    n1 = n2.operator+(n3);    // n2 + n3 = 3/4 + 2/3 = 9/12 + 8/12 = 17/12
    displayNumbers("After ", n1, n2, n3);

    cout << endl << endl << "**** n1 = n2 += n3; // n1 and n2 should become 17/12. Others unchanged" << endl;
    initializeNumbers (n1, n2, n3);
    displayNumbers("Before", n1, n2, n3);
    n1 = n2 += n3;
    displayNumbers("After ", n1, n2, n3);

    cout << endl << endl << "**** n1 = n2 - n3;  // n1 should be 1/12. Others unchanged" << endl;
    initializeNumbers (n1, n2, n3);
    displayNumbers("Before", n1, n2, n3);
    n1 = n2 - n3;              	// n2 - n3 = 3/4 - 2/3 = 9/12 - 8/12 = 1/12
    displayNumbers("After ", n1, n2, n3);

    cout << endl << endl << "**** n1 = n2 -= n3;  // n1 and n2 should be 1/12. Others unchanged" << endl;
    initializeNumbers (n1, n2, n3);
    displayNumbers("Before", n1, n2, n3);
    n1 = n2 -= n3;
    displayNumbers("After ", n1, n2, n3);

    cout << endl << endl << "**** Rational number to double. 1/12 displays as 0.0833333" << endl;
    cout << "double(n2) = " << double(n2) << endl;
	cout << endl;
}

// Initialize each of the variables before testing each rational operator
template <class T>
void initializeNumbers (Rational<T> &n1, Rational<T> &n2, Rational<T> &n3)
{
    n1 = Rational<T> ();     //    0  no arguments
    n2 = Rational<T> (3,4);  //   3/4
    n3 = Rational<T> (2,3);  //   2/3
}

// Display each of the rational numbers using the friend function <<
template <class T>
void displayNumbers (const char *msg, Rational<T> n1, Rational<T> n2, Rational<T> n3)
{
    cout << msg << " " << n1 << "\t" << n2 << "\t" << n3 << endl;
}
template <class T>
void unitTestRational()
{
  unitTestRationalNumbers<T>();
  unitTestTelescoping <T>();
}


void unitTestUnaryNegate () {
  // __func__ is the invisible C++ local variable
  // data type is string
  // which means the name of the current function


  cout << "Enter the function " << __func__ << endl;

  Rational <int> x (3,2);
  cout << x << endl; // print out Rational number x

  // test unary +:  print out +x
  // only if you have implemented (overloaded) it.
  cout << "+x = " << +x << endl;

  // test unary -:  print out -x
  cout << "-x = " << -x << endl;
}

/*
  This unit test is for the overloaded operator == 
  It is a template function that can accept any data type T
  e.g., T can be int, short, long long, and even double.
*/
// T is the data type used by the RAtional

template<class T> 
void unitTestOperatorEquivalence () {
  cout << "Enter the function " << __func__ << endl;

  Rational < T > x ( 1, 2);   // T is the data type
  Rational < T > y ( -2,-4);   // T is the data type
  Rational < T > z ( 1,4);   // T is the data type

  cout << "x = " << x << endl;
  cout << "y = " << y << endl;
  cout << "z = " << z << endl;
  cout << "x==y??? " << boolalpha<< (x==y) << endl;
  cout << "x==z??? " << boolalpha<< (x==z) << endl;
}

template<class T>
 Rational<T>  
RandomRationalNumber () { 
  T n = 6;
  T a = rand()%n - n/2 ;
  T b = rand()%n - n/2 ;
  return Rational<T>  ( a,b );
}

template<class T>
void unitTestRandomRational( ) {
  using ItemType = Rational<T> ; // ItemType is an alias of Rational<T>

  cout << endl << endl;
  cout << "Entering function " << __func__ << std::endl;

  T n = 10;

  // create a vector of n elements of 2/3 
  // The following declaration will be equivalent to the block below it.
  vector < ItemType >  v(n, ItemType{2,3} ) ;
  generate (v.begin(), v.end(), RandomRationalNumber<T>);

// Find the min and max elements in the vector
  auto beginIter = cbegin( v );
  auto endIter = cend( v );

  auto res = minmax_element(begin(v) , end(v) );
  cout << "min is " << *(res.first) << " and max is " << *(res.second) << endl;
  auto it = adjacent_find(beginIter, endIter);
  if (it != endIter) {
    cout << "Found two consecutive equal elements with value " << *it << endl;
  }


  
  CompareRational<T> cmp;
  cmp.m_mode = 0;
  sort (v.begin(), v.end(), cmp  );
  cmp.m_mode = 1;
  sort (v.begin(), v.end(), cmp  );
  // reverse (v.begin(), v.end()  );
  // sort (v.begin(), v.end(), less<T>() );
  
  multiset<ItemType> xset (v.begin(), v.end());

  for ( auto& e: xset ) {
    cout << e << " ";
  }
  cout << endl;

  // The following basically is doing something like this
  ItemType total {0} ;
  for ( auto& e: v ) total  += e ;
  
  ItemType sum{0} ;
  sum = accumulate(v.begin(), v.end(), sum );
  ItemType product = accumulate(v.begin(), v.end(), 1, multiplies<ItemType>());


  std::random_shuffle(v.begin(), v.end());


  // The following are doing something like this
  // for ( auto& e: xset) cout << e << " < " ;
  ostream_iterator<ItemType> temp (std::cout, " < ");

  std::copy(xset.begin(), --xset.end(), temp );


  ostream_iterator<ItemType> temp2 (std::cout, "");
  std::copy(--xset.end(), xset.end(), temp2 );

  cout << endl<< endl;
  cout << "The sum is : " << sum << endl;
  cout << "The product is : " << product << endl;
  cout << "The average is : " << sum/Rational<int>(xset.size()) << endl;
  cout << endl;
/*
*/


}
/*
  Compute the product p as = (1/2)*(2/3)* ... * (n/(n+1) = 1/(n+1)
*/
template<class T>
void unitTestRationalNumberProduct(T n) {
  Rational<T> p (1,1);
  for (T i = 0; i < n; ++i ) {
    p *= Rational<T> ( i+1, i+2) ;
  }
  cout << "The product = " << p << endl;
}

