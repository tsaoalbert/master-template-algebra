#pragma once

// #include "stdafx.h" // use only for Microsoft Visual Studio C++
#include <iostream>
using namespace std;

/*
Rules of Arithmetic Operators for Rationl Numbers
  Given x = a/b, y = c/d,

  x+y = (a*d+c*b) / ( b*d)
  x-y = (a*d-c*b) / ( b*d)
  x*y = (a*c) / ( b*d)
  x/y = (a*d) / ( b*c)

  x==y  iff   a*d==c*b
  x<=y  iff   a*d<=c*b
*/

/*
TODO: Implement the following overloaded operators for Rational Numbers.
a)  Arithmetic operators +,-,*,/,+=,-=,*=,/=
b)  Unary operators +,-
c)  Relational operators ==, !=, <, <=, >, >= , and
d)  Prefix and Postfix Increment/Decrement ++ -- 
e)  Big 3: Copy Cstr, Assignment operator=,  and Dstr


*/
template<class T>
class Rational ;


// function object
template<class T>
class CompareRational {
public:
  int m_mode ;
  bool operator() ( const Rational<T>& x, const Rational<T>& y) {
    if ( m_mode==0 ) {
      return x < y ;
    } else {
      return x > y ;
    }
  } ;
};

template<class T>
ostream &operator<< (ostream &out, const Rational<T> &r);

template<class T>
class Rational
{
    // Friend functions are actually declared outside the scope of the
    // class but have the right to access public and private data and
    // member function members that belong to the class. The friend
    // function below gives the << operator for ostreams (including cout)
    // the ability to output a Rational object by accessing its member data.

    friend ostream &operator<< <T> (ostream &out, const Rational<T> &r);

public:

  //TODO: add assignment operator=
	Rational<T> operator= (const Rational<T>& right);   // += addition assignment operator

  //TODO: add copy cstr

  // default  cstr
	Rational<T> (T num=0, T denom=1);  // also provides default constructor

  Rational<T> add (const Rational<T>& ) const ;
	Rational<T> operator+  (const Rational<T> &) const ;    // + addition operator
	Rational<T> operator+= (Rational<T> right);   // += addition assignment operator
  Rational<T> operator- (const Rational<T>& right) const ;
	Rational<T> operator-= (Rational<T> right);   // += addition assignment operator

  //TODO: add binary arithmetic overloadded operators *, *=, /, /=, 
  Rational<T> operator/ (const Rational<T>& right) const ;
  //TODO: add unary  arithmetic overloadded operators -, +
  //TODO: add overloadded relational operators ==, <=, >=, !=

  //TODO: add helper function to_string 
  string to_string () const ;

  operator double() const; // convert Rational to double

  //TODO: add friend overloadded insertion  operator << 
  //TODO: add friend overloadded extraction  operator >> 

private:
	T numerator;
	T denominator;
    // helper functions are private and not accessible by the main program
  T GCD(T v1, T v2);
   Rational<T> setRational (T n, T d);
};


/*
TODO:
  implement the following overloaded operators,
    *, *=,
    /, /=,
    ==, !=,
    <, <=,
    >, >= ,
    operator float() // convert Rational to float
*/

// By using the default parameter settings in Rational.h, this
// constructor also provides the default constructor Rational()
template<class T>
Rational<T>::Rational (T num, T denom)
{ 
    setRational(num,denom);   // set numerator and denominator, reduce fraction, fix the sign 
}

// Helper function to fix a zero denominator and fix the sign if denominator is negative
template<class T>
Rational<T> Rational<T>::setRational (T n, T d) // helper function
{
    // if d == 0 then set d = 1
    if (d == 0) {
      d = 1;
      n = 0 ;
    }

    numerator = n;
    denominator = d;

    if ( denominator < 0 ) // if denominator is neg, multiply num and denom by -1
    {
        numerator = -numerator;     // fix sign of numerator +/-
        denominator = -denominator; // denominator always +
    }

    T gcd = GCD(numerator, denominator);
    numerator /= gcd;
    denominator /= gcd;
    return *this;   // return the current object
}

// find the lowest common divisor using a recursive function
template<class T>
T mygcd (T v1, T v2)
{
    if (v2==0) return v1;
    else return mygcd  (v2, v1%v2);
}

// find the lowest common divisor using a recursive function
template<class T>
T Rational<T>::GCD(T v1, T v2)
{
    if (v1<0) v1 = -v1;
    if (v2<0) v2 = -v2;
    if (v2==0) return v1;

    long long a = v1;
    long long b = v2;
    if ( a==v1 && b==v2 ) {
      return mygcd  (b, a%b);
    } else {
      return 1;
    } 
}

template<class T>
Rational<T> Rational<T>::add (const Rational<T>& y) const
{
	// create local (temporary) variables
  T a = numerator;
	T b = denominator;
  T c = y.numerator;
	T d = y.denominator;

  return Rational<T>(a*d+b*c, b*d);
}

// the operator+ method does the same thing as the add method
template<class T>
Rational<T> Rational<T>::operator+ (const Rational<T>& y) const
{
  return add( y);
}

template<class T>
Rational<T> Rational<T>::operator= (const Rational<T> &right)
{
  numerator = right.numerator;
	denominator = right.denominator ;
  return *this;
}

template<class T>
Rational<T> Rational<T>::operator+= (Rational<T> right)
{
    // the current object is updated with the result of the += 
    numerator = numerator*right.denominator + right.numerator*denominator;
	denominator = denominator * right.denominator;

    // fix the sign, reduce the fraction and return the current object
    return setRational(numerator, denominator);
}

// the operator- method does the same thing as the add method
template<class T>
Rational<T> Rational<T>::operator- (const Rational<T>& right) const
{
  // compute the result and save in the local variables
  // the current object's numerator and denominator are not changed
	T a = numerator*right.denominator - right.numerator*denominator;
	T b = denominator * right.denominator;

  // create a new Rational object with the result and return it
  return Rational<T>(a, b);
}

template<class T>
Rational<T> Rational<T>::operator-= (Rational<T> right)
{
    // the current object is updated with the result of the -= 
	numerator = numerator*right.denominator - right.numerator*denominator;
	denominator = denominator * right.denominator;

    // fix the sign, reduce the fraction and return the current object
    return setRational(numerator, denominator);
}

template<class T>
Rational<T>::operator double() const // convert Rational to double and return
{
    return double(numerator) / double(denominator);
}

template<class T>
string  Rational<T>::to_string () const
{
	return to_string (numerator) + '/' + to_string (denominator);
}

template<class T>
ostream &operator<< (ostream &out, const Rational<T> &r)
{ 
    out << r.numerator ;
    if ( r.denominator != 1 ) {
      out << '/' << r.denominator; 
    }
    return out; // This is to keep the stream flowing
}

// the operator- method does the same thing as the add method
template<class T>
Rational<T> Rational<T>::operator/ (const Rational<T>& right) const
{
  // compute the result and save in the local variables
  // the current object's numerator and denominator are not changed
  T a = numerator*right.denominator ;
  T b = denominator * right.numerator;

  // create a new Rational object with the result and return it
  return Rational<T>(a, b);
}
